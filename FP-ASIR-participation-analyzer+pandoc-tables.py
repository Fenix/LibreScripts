"""
Rationale: Analyze ASIR student access patterns with robust error handling,
          logging, and optimized data processing. Uses functional programming 
          with bash tools integration (pandoc) and proper exception management.
Version: 0.2-alpha
Author: fenix & LLM friends
License: GPL (GNU General Public License)

Longer Rationale :
  The script offers:

Access pattern analysis showing:

Average time since last access
Longest/shortest access gaps
Student grouping by access time ranges


Export options using pandoc for:

JSON (for API integration)
XML (for document systems)
CSV (for spreadsheets)


"""

import subprocess
import json
import re
import logging
import os
from datetime import datetime, timedelta
from typing import List, Dict, Tuple, Optional
from functools import reduce, lru_cache
from dataclasses import dataclass
from pathlib import Path

# Set up logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    handlers=[
        logging.FileHandler('asir_analyzer.log'),
        logging.StreamHandler()
    ]
)

@dataclass
class StudentRecord:
    """Immutable data class for student records"""
    name: str
    email: str
    last_access: str
    hours_since_access: int

class ASIRAnalyzerError(Exception):
    """Custom exception for ASIR Analyzer errors"""
    pass

@lru_cache(maxsize=128)
def parse_time_span(time_str: str) -> int:
    """Parse Spanish time format to hours with caching for performance
    Example: '2 días 1 hora' -> 49 hours"""
    try:
        if not isinstance(time_str, str):
            raise ASIRAnalyzerError(f"Invalid time format: {time_str}")

        days = hours = 0
        time_str = time_str.lower().strip()
        
        day_match = re.search(r'(\d+)\s*días?', time_str)
        hour_match = re.search(r'(\d+)\s*hora[s]?', time_str)
        
        if day_match:
            days = int(day_match.group(1))
        if hour_match:
            hours = int(hour_match.group(1))
            
        return days * 24 + hours
    except Exception as e:
        logging.error(f"Error parsing time span '{time_str}': {str(e)}")
        raise ASIRAnalyzerError(f"Could not parse time format: {time_str}")

def check_pandoc_installation() -> bool:
    """Verify pandoc is installed and accessible"""
    try:
        result = subprocess.run(['pandoc', '--version'], 
                              capture_output=True, 
                              text=True)
        return result.returncode == 0
    except FileNotFoundError:
        return False

def load_data_from_markdown(markdown_file: str) -> List[StudentRecord]:
    """Convert markdown to structured data with error handling"""
    try:
        if not os.path.exists(markdown_file):
            raise ASIRAnalyzerError(f"File not found: {markdown_file}")
            
        # Validate file content
        with open(markdown_file, 'r', encoding='utf-8') as f:
            lines = f.readlines()
            
        if len(lines) < 3:  # Header + separator + at least one data row
            raise ASIRAnalyzerError("Invalid markdown file format")
            
        data = []
        for line in lines:
            if '|' in line and not line.startswith('|:'):
                cols = [col.strip() for col in line.split('|')[1:-1]]
                if len(cols) >= 3 and cols[0] and cols[1]:  # Skip empty rows
                    try:
                        record = StudentRecord(
                            name=cols[0],
                            email=cols[1],
                            last_access=cols[2],
                            hours_since_access=parse_time_span(cols[2])
                        )
                        data.append(record)
                    except Exception as e:
                        logging.warning(f"Skipping invalid record: {cols}, Error: {str(e)}")
                        
        if not data:
            raise ASIRAnalyzerError("No valid data found in file")
            
        return data
        
    except Exception as e:
        logging.error(f"Error loading data from {markdown_file}: {str(e)}")
        raise ASIRAnalyzerError(f"Failed to load data: {str(e)}")

def analyze_access_patterns(data: List[StudentRecord]) -> Dict:
    """Analyze access patterns with error handling"""
    try:
        if not data:
            raise ASIRAnalyzerError("No data available for analysis")

        # Calculate statistics using reduce with validation
        stats = reduce(lambda acc, student: {
            'total_hours': acc['total_hours'] + student.hours_since_access,
            'max_hours': max(acc['max_hours'], student.hours_since_access),
            'min_hours': min(acc['min_hours'], student.hours_since_access),
            'students': acc['students'] + 1
        }, data, {'total_hours': 0, 'max_hours': 0, 'min_hours': float('inf'), 'students': 0})

        # Validate statistics
        if stats['students'] == 0:
            raise ASIRAnalyzerError("No valid student records for analysis")

        # Define access ranges
        ranges = {
            '< 2 days': lambda h: h < 48,
            '2-3 days': lambda h: 48 <= h < 72,
            '3-4 days': lambda h: 72 <= h < 96,
            '4+ days': lambda h: h >= 96
        }

        # Group students by access time
        access_groups = {
            range_name: len(list(filter(
                lambda x: condition(x.hours_since_access), data)))
            for range_name, condition in ranges.items()
        }

        return {
            'average_hours': round(stats['total_hours'] / stats['students'], 2),
            'max_hours': stats['max_hours'],
            'min_hours': stats['min_hours'],
            'access_groups': access_groups,
            'total_students': stats['students']
        }

    except Exception as e:
        logging.error(f"Error analyzing access patterns: {str(e)}")
        raise ASIRAnalyzerError(f"Analysis failed: {str(e)}")

def export_format(data: List[StudentRecord], 
                 format_type: str, 
                 output_file: str) -> bool:
    """Export data to specified format with enhanced error handling"""
    temp_file = Path('temp.md')
    try:
        if not data:
            raise ASIRAnalyzerError("No data to export")
            
        if not check_pandoc_installation():
            raise ASIRAnalyzerError("Pandoc is not installed or accessible")
            
        # Validate format
        formats = {
            'json': 'json',
            'xml': 'docbook',
            'csv': 'csv'
        }
        
        if format_type not in formats:
            raise ASIRAnalyzerError(f"Unsupported format: {format_type}")
            
        # Create markdown content
        markdown = "| Name | Email | Last Access | Hours Since Access |\n"
        markdown += "|------|--------|--------------|------------------|\n"
        
        for student in data:
            markdown += (f"| {student.name} | {student.email} | "
                       f"{student.last_access} | {student.hours_since_access} |\n")
            
        # Write temporary file
        temp_file.write_text(markdown, encoding='utf-8')
            
        # Convert using pandoc
        cmd = f"pandoc {temp_file} -f markdown -t {formats[format_type]} -o {output_file}"
        result = subprocess.run(cmd, 
                              shell=True, 
                              capture_output=True, 
                              text=True)
        
        if result.returncode != 0:
            raise ASIRAnalyzerError(f"Pandoc conversion error: {result.stderr}")
            
        return True
        
    except Exception as e:
        logging.error(f"Export error: {str(e)}")
        raise ASIRAnalyzerError(f"Export failed: {str(e)}")
        
    finally:
        # Cleanup temporary file
        if temp_file.exists():
            temp_file.unlink()

def main():
    """Main function with error handling"""
    print("ASIR Access Pattern Analyzer (Alpha Version)")
    print("------------------------------------------")
    
    try:
        # Initial checks
        if not check_pandoc_installation():
            print("Warning: Pandoc not found. Export features will be disabled.")
        
        # Load data
        data = load_data_from_markdown('paste.txt')
        
        while True:
            try:
                print("\nOptions:")
                print("1. Analyze access patterns")
                print("2. Export data (JSON/XML/CSV)")
                print("3. Exit")
                
                choice = input("\nSelect an option (1-3): ").strip()
                
                if choice == "1":
                    analysis = analyze_access_patterns(data)
                    print("\nAccess Pattern Analysis:")
                    print(f"Total students: {analysis['total_students']}")
                    print(f"Average time since last access: {analysis['average_hours']} hours")
                    print(f"Longest time without access: {analysis['max_hours']} hours")
                    print(f"Most recent access: {analysis['min_hours']} hours ago")
                    print("\nAccess Groups:")
                    for range_name, count in analysis['access_groups'].items():
                        print(f"{range_name}: {count} students")
                        
                elif choice == "2":
                    if not check_pandoc_installation():
                        print("Error: Pandoc is required for export functionality.")
                        continue
                        
                    format_type = input("Enter export format (json/xml/csv): ").lower()
                    if format_type in ['json', 'xml', 'csv']:
                        output_file = f"asir_data.{format_type}"
                        export_format(data, format_type, output_file)
                        print(f"\nData exported to {output_file}")
                    else:
                        print("\nInvalid format!")
                        
                elif choice == "3":
                    print("Goodbye!")
                    break
                    
                else:
                    print("Invalid option. Please try again.")
                    
            except ASIRAnalyzerError as e:
                print(f"\nError: {str(e)}")
                logging.error(str(e))
            except Exception as e:
                print("\nAn unexpected error occurred!")
                logging.exception("Unexpected error in main loop")
                
    except Exception as e:
        print(f"\nCritical error: {str(e)}")
        logging.critical(f"Critical error in main: {str(e)}")
        return 1
        
    return 0

if __name__ == "__main__":
    exit(main())
