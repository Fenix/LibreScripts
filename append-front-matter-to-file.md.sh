#!/bin/bash
# rationale : Append front matter to file, hence concatenate two files, actually 
# version 0.1
# license GPLv3 

# Prompt the user for source and output folder paths
read -p "Enter the source folder path: " source_folder
read -p "Enter the output folder path: " output_folder
read -p "Enter the file1.md folder path: " file1_folder
read -p "Enter the file1.md filename: " file1_name

# Concatenate the contents of frontmatter.md and file1.md into temp
cat "$source_folder/frontmatter.md" "$file1_folder/$file1_name" > temp

# Replace file1.md with the contents of temp
mv temp "$output_folder/$file1_name"

# the script prompts the user for the source and output folder paths as before. Additionally, it asks for the folder path and filename of `file1.md`. By taking both the folder and name as variables, the script provides greater flexibility in specifying the location and name of `file1.md`.
