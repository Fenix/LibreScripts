#!/bin/bash
# To append a '## stitle' line at the beginning of all  files in a folder
# folder="/path/to/folder"

folder="/tmp/stories2"


# Loop through all markdown files in the folder
for file in "$folder"/*.txt; do
    # Add the '## stitle' line at the beginning
    sed -i '1s/^/## stitle\n\n/' "$file"
done
