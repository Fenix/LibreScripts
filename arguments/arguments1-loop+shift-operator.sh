#!/bin/bash

# playing with arguments in Bash - " loop +
#    the shift operator causes the indexing of the input to start from the shifted position[..]"
# source : https://www.baeldung.com/linux/use-command-line-arguments-in-bash-script

# instead of $@ :
#
# for user in "$@" 
# do
#     echo "Username - $i: $user";
#     i=$((i + 1));
# done

# we now use :

i=1;
j=$#;
while [ $i -le $j ]
do
    echo "Username - $i: $1";
    i=$((i + 1));
    shift 1;
done
