#!/bin/bash

# playing with arguments in Bash - loop 
# source : https://www.baeldung.com/linux/use-command-line-arguments-in-bash-script

for user in "$@" 
do
    echo "Username - $i: $user";
    i=$((i + 1));
done
