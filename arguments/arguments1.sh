#!/bin/bash

# playing with arguments in Bash
# source : https://www.baeldung.com/linux/use-command-line-arguments-in-bash-script

echo "Username: $1";
echo "Age: $2";
echo "Full Name: $3";
