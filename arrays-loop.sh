#!/bin/bash
# rationales= playing / looping on arrays
# source = https://dev.to/drraq/bash-all-you-need-to-know-to-work-with-bash-arrays-3fna

declare -A stars=([ALP]="Cow" [BET]="Bow" [GAM]="Goat" [DEL]="Scorpion"
[EPS]="Balance" [ZET]="Queen" [ETA]="Lion" [THE]="Fish")

for key in ${!stars[@]}; do
    echo "$key => ${stars[$key]}"
done

# Output
# GAM => Goat
# EPS => Balance
# ALP => Cow
# ZET => Queen
# DEL => Scorpion
# BET => Bow
# ETA => Lion
# THE => Fish

# Get length of stars

echo ${#stars[@]}

