#!/bin/bash
#
# playing - jugando con arrays asociativos en Bash - 3
# source : https://dev.to/drraq/bash-all-you-need-to-know-to-work-with-bash-arrays-3fna


declare -A stars=([ALP]="Cow" [BET]="Bow" [GAM]="Goat" [DEL]="Scorpion"
[EPS]="Balance" [ZET]="Queen" [ETA]="Lion" [THE]="Fish")

for key in ${!stars[@]}; do
    echo "$key => ${stars[$key]}"
done




# # OR
# declare -A planets
# planets["first"]="Mercury"
# planets["second"]="Venus"
# planets["third"]="Earth"
# planets["Musk adventure"]="Mars"
# planets["second last"]="Uranus"

