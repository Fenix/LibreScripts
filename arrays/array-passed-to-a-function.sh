#!/bin/bash
#
# playing - jugando con arrays en Bash - 2
# source : https://www.baeldung.com/linux/bash-array-function


# Function that receives an array argument
print_elements() {
    local my_array=("$@")  # Assign the passed array to a local array variable

    # Print array elements
    echo "First element: ${my_array[0]}"
    echo "First element: ${my_array[1]}"
    echo "First element: ${my_array[2]}"
    echo "First element: ${my_array[3]}"
}

# Declare array
array=(1 2 3 4)

# Call the function and call the array as input 
print_elements "${array[@]}"



