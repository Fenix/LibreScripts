#!/bin/bash
#
# playing - jugando con arrays en Bash
# source : https://dev.to/drraq/bash-all-you-need-to-know-to-work-with-bash-arrays-3fna

months=("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")


# echo ${months[9]}
# # Output: Oct

index=11

echo ${months[$index]}
#Output: Jun
