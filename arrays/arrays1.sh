#!/bin/bash
#
# playing - jugando con arrays en Bash
# source : https://dev.to/drraq/bash-all-you-need-to-know-to-work-with-bash-arrays-3fna

months=("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")

# Get all values from months array
echo ${months[*]}






# or echo ${months[@]}
# Output: Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec

# # Get all indexes
# echo ${!months[@]}
# # or echo ${!months[*]}
# # Output: 0 1 2 3 4 5 6 7 8 9 10 11

# # Length of array
# echo ${#months[@]}
# # or echo ${#months[*]}
# # Output: 12
