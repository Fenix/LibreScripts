#!/bin/bash
#
# playing - jugando con arrays en Bash - 2
# source : https://dev.to/drraq/bash-all-you-need-to-know-to-work-with-bash-arrays-3fna


months=("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")

# Add to the months array
months+=("Raj" "Sha" "Ra1" "Ra2")

echo ${months[@]}
# Output: Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec Raj Sha Ra1 Ra2

# Add element to any specified index
months[20]="Muh"
echo ${months[@]}
# Output: Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec Raj Sha Ra1 Ra2 Muh


# Get all indexes
echo ${!months[@]}
# Output: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 20

# Initialize second array
animals=("Cow" "Goat" "Horse" "Lion" [11]="Brock Lesnar")

# Get all indexes for animals
echo ${!animals[@]}
# Output: 0 1 2 3 11
