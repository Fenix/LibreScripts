=Scripts for Pandoc conversion automation=
* fecha

#+name: fecha
#+begin_src bash :output :results org dir ~/Desktop
date
#+end_src


* pandoc

#+name: pandoc-org2md
#+begin_src bash :output :results org dir ~/Org
# pandoc export
#+end_src


* pandoc docx to MD

 To convert a .docx file to strict markdown using pandoc in GNU Bash, you can use the following command:

```bash
pandoc -s input.docx -t markdown_strict -o output.md
```


#+name: pandoc-docx2md
#+begin_src bash :output :results org dir ~/Org/conversion 
# pandoc export
pandoc -s input.docx -t markdown_strict -o output.md
#+end_src




* pandoc-html-md

#+name: Pandoc-html-md
#+begin_src bash :output :results org dir ~/Desktop
curl --silent https://pandoc.org/installing.html | pandoc --from html --to markdown_strict -o output.md
#+end_src

#+RESULTS: Pandoc-html-md
#+begin_src org
#+end_src

* pandoc-md-html

#+name: Pandoc-md-html
#+begin_src bash :output :results org dir ~/Desktop
      echo "  INPUT=to-htmlize.md"
      INPUT_FILE=/tmp/to-htmlize.md
#   cat $INPUT_FILE

    #    pandoc $INPUT_FILE --from md --to html -o htmlized-md.html

      pandoc $INPUT_FILE --from markdown  --to html 


#   cat /tmp/htmlized-md.html

#+end_src

#+RESULTS: Pandoc-md-html
#+begin_src org
  INPUT=to-htmlize.md
<h2 id="why-do-our-customers-love-ford-protect">Why Do Our Customers 
Love Ford Protect?</h2>
<h5 id="genuine-ford-parts-and-service">genuine ford parts and
service</h5>
<p>Factory trained &amp; certified techs make all repairs with authentic
Ford parts.</p>
<h5 id="easy-to-transfer-or-cancel">easy to transfer or cancel</h5>
<p>Ford Protect Extended Service Plans are 100% transferable and may
increase the resale value of your vehicle</p>
<h5 id="accepted-at-5000-ford-dealers">accepted at 5000+ ford
dealers</h5>
<p>Wherever you find yourself in the U.S., Ford Protect is accepted at
all Ford and Lincoln dealers.</p>
<h5 id="hour-roadside-assistance">24-Hour roadside assistance</h5>
<p>Gain access to a toll-free hotline for roadside assistance any time
you need it.</p>
<h5 id="rental-vehicle-benefits">Rental Vehicle Benefits</h5>
<p>When a vehicle is kept overnight for an Ford Protect repair, your
plan gives you up to 10 days of rental vehicle benefits.</p>
<h5 id="million-service-plans-sold">35 Million Service Plans Sold</h5>
<p>Since 1976 Ford ESPs have provided peace-of-mind protection for Ford
and Lincoln owners.</p>
#+end_src


