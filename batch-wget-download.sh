#! /bin/bash
# version 0.1
# author: fenix
# license: WTFL
# rationale :
#    easily download a list of image URLs (file) using a Bash script with the help of wget 

while IFS= read -r url; do
  wget "$url"
done < image_urls.txt
