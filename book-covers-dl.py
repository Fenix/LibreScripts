# rationale:
#    This script downloads each book cover image from the VU University Press catalogue pages and saves it locally:
# Loop through all catalogue pages: The script iterates over pages 1 to 11 of the catalogue.
# Download each page: The requests library is used to download the HTML content of each page.
# Parse HTML to find book cover images: Using Beautiful Soup, the script locates each book cover image (img tags with class attachment-woocommerce_thumbnail).
# Download and save images: Each book cover is saved in the vu_book_covers folder.

# version 0.3
# license : WTFL

import requests
import os
import bs4
import time
from urllib.parse import urlparse, urljoin
import re

# Base URL and page range for VU University Press catalogue
base_url = 'https://vuuniversitypress.com/?lang=en&product-page='
os.makedirs('vu_book_covers', exist_ok=True)  # Create a folder to store book covers

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'
}

for page_num in range(1, 12):
    try:
        # Construct the URL for each page
        url = f'{base_url}{page_num}'
        print(f'Downloading page {url}...')
        res = requests.get(url, headers=headers)
        res.raise_for_status()
        
        soup = bs4.BeautifulSoup(res.text, 'html.parser')
        
        # Find the URL of each book cover image
        bookElems = soup.select('img.attachment-woocommerce_thumbnail')
        if not bookElems:
            print(f'No book images found on page {page_num}.')
        else:
            for bookElem in bookElems:
                bookUrl = bookElem.get('src')
                if bookUrl:
                    # Modify URL to get the non-scaled version of the image by removing any scaling suffix
                    bookUrl = re.sub(r'-\d+x\d+(?=\.[a-zA-Z]{3,4}$)', '', bookUrl)
                    bookUrl = urljoin(url, bookUrl)  # Ensures the URL is absolute
                    # Download the image
                    print(f'Downloading image {bookUrl}...')
                    try:
                        img_res = requests.get(bookUrl, headers=headers)
                        img_res.raise_for_status()
                        
                        # Unique image naming based on the full path
                        imageFileName = os.path.basename(urlparse(bookUrl).path)
                        unique_name = f'{page_num}_{imageFileName}'
                        with open(os.path.join('vu_book_covers', unique_name), 'wb') as imageFile:
                            for chunk in img_res.iter_content(100000):
                                imageFile.write(chunk)
                    except requests.RequestException as e:
                        print(f'Failed to download image {bookUrl}: {e}')
                        
        # Be polite with the server - rate limit
        time.sleep(1)
        
    except requests.RequestException as e:
        print(f'Failed to download page {url}: {e}')

print('Done.')
