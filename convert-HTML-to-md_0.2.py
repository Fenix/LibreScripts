# rationale :
##  Web Scrap -> import takes a URL as input. We'll add requests to fetch the webpage content
# version 0.2
# license : GPLv3
# changes:
# - Improved error handling with retries and detailed messages
# - Enhanced filename generation with timestamp and sanitization
# - Added logging functionality
# - Batch processing of URLs with parallelization
# - Command-line arguments for flexibility
# - Cleaned up HTML using BeautifulSoup for better Markdown conversion

import html2text
import requests
from urllib.parse import urlparse
import os
import logging
from typing import Optional
from datetime import datetime
import re
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor
import argparse

def setup_logging():
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def fetch_webpage(url: str) -> Optional[str]:
    """Fetch HTML content from URL with error handling and retry"""
    for attempt in range(3):  # Retry up to 3 times
        try:
            response = requests.get(url, timeout=10)
            response.raise_for_status()
            return response.text
        except requests.RequestException as e:
            logging.warning(f"Attempt {attempt + 1} failed: {e}")
            time.sleep(2)  # Wait before retrying
    logging.error("Failed to fetch webpage after 3 attempts.")
    return None

def clean_html(html_content: str) -> str:
    """Remove unnecessary elements like scripts and styles"""
    soup = BeautifulSoup(html_content, "html.parser")
    for tag in soup(["script", "style"]):
        tag.decompose()
    return soup.prettify()

def html_to_md(html_content: str) -> str:
    """Convert HTML string to Markdown"""
    converter = html2text.HTML2Text()
    converter.ignore_links = False
    converter.ignore_images = False
    converter.body_width = 0
    return converter.handle(html_content)

def get_filename_from_url(url: str) -> str:
    """Generate a sanitized and timestamped filename from URL"""
    parsed = urlparse(url)
    path = parsed.path.strip('/')
    name = re.sub(r'[^\w\-_\.]+', '_', path) or parsed.netloc
    timestamp = datetime.now().strftime('%Y%m%d_%H%M%S')
    return f"{name}_{timestamp}"

def convert_url_to_markdown(url: str, output_dir: str = "converted_md") -> None:
    """Main function to convert webpage to markdown"""
    # Create output directory if it doesn't exist
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Fetch HTML content
    html_content = fetch_webpage(url)
    if not html_content:
        return

    # Clean up HTML
    clean_content = clean_html(html_content)

    # Convert to markdown
    md_content = html_to_md(clean_content)

    # Generate output filename
    filename = get_filename_from_url(url)
    output_path = os.path.join(output_dir, f"{filename}.md")

    # Save markdown file
    try:
        with open(output_path, 'w', encoding='utf-8') as f:
            f.write(md_content)
        logging.info(f"Successfully converted! Saved to: {output_path}")
    except IOError as e:
        logging.error(f"Error saving file: {e}")

def batch_convert(urls: list[str], output_dir: str = "converted_md") -> None:
    """Convert multiple URLs in parallel"""
    with ThreadPoolExecutor() as executor:
        executor.map(lambda url: convert_url_to_markdown(url, output_dir), urls)

def main():
    """Entry point for command-line usage"""
    setup_logging()
    parser = argparse.ArgumentParser(description="Convert a webpage to Markdown")
    parser.add_argument("url", nargs="*", help="URL(s) of the webpage(s) to convert")
    parser.add_argument("-o", "--output-dir", default="converted_md", help="Output directory")
    args = parser.parse_args()

    if args.url:
        batch_convert(args.url, args.output_dir)
    else:
        logging.error("No URLs provided. Exiting.")

if __name__ == "__main__":
    main()
