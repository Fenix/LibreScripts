#!/bin/bash

# rationale : delete-lines-X-to-Y script , e.g.: lines 4 to 8 in the front-matter
      prompt the user for X and Y values
# version : 0.1
#
# inspiration/source : https://www.baeldung.com/linux/use-command-line-arguments-in-bash-script
# 
#
#

while getopts x:y: flag
do
    case "${flag}" in
	x) X=${OPTARG};;
	y) Y=${OPTARG};;
    esac
done

echo "Lines range to be removed:"
echo "LineX: $X";
echo "LineY: $Y";
echo "Lines $X to $Y will be removed, ..(make sure you commited the last changes)"

# # Let's loop on the folder 

# DPATH="/tmp/dfo2017/_posts/*.md"
# for f in $DPATH
# do
#   if [ -f $f -a -r $f ]; then
#       sed -i "$X,$Yd" "$f"
#   else
#    echo "Error: Cannot read $f"
#   fi
# done


# # TADA prompt the user for DPATH argument, prior to 'Let's loop on the folder ' !
