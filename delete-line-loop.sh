#!/bin/bash
# rationale: massage markdown front-matter 
#    delete a given line X 
# 
# inspiration/source :
# https://www.cyberciti.biz/faq/unix-linux-replace-string-words-in-many-files/
# 
# license: GPLv3
# script version 0.2
# TADA: refactor with something like
#   $ line=3 sed -i "/$line/d" sample
# so that we can take 'line'  as the script argument!?
# TADA2 - so that we pass the argument to the script while exec.


DPATH="/tmp/dfo2017/_posts/*.md"
for f in $DPATH
do
  if [ -f $f -a -r $f ]; then
      sed -i '4,5d' "$f"
  else
   echo "Error: Cannot read $f"
  fi
done

