#!/bin/bash

# rationale : delete-lines-X-to-Y script , e.g.: lines 4 to 8 in the front-matter
# version : 0.1
#
# inspiration/source : https://www.baeldung.com/linux/use-command-line-arguments-in-bash-script
# 
#
#

while getopts u:a:f: flag
do
    case "${flag}" in
	u) X=${OPTARG};;
	a) Y=${OPTARG};;
    esac
done

echo "LineX: $username ";
echo "LineY: $age";
