#!/bin/sh
# https://stackoverflow.com/questions/5609192/how-to-set-up-tmux-so-that-it-starts-up-with-specified-windows-opened
tmux new-session -d 'KL3GG1TB'
tmux split-window -v 'MUSIC'
tmux split-window -h
tmux new-window 'MDN'
tmux new-window 'HUGOCODEX'
tmux split-window -h
tmux -2 attach-session -d
