#! /bin/sh
# por fenix ; enciende ambos: la pantall del portátil como el monitor VGA externo con xrandr i3wm
# SO : GNU Guix Linux

xrandr --output LVDS-1 --primary --auto --pos 0x1024 --rotate normal --output VGA-1 --auto  --pos 0x0 --rotate normal --output HDMI-1 --off --output DP-1 --off --output HDMI-2 --off --output HDMI-3 --off --output DP-2 --off --output DP-3 --off

