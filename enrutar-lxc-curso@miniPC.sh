#!/bin/bash
# rationale : lxc -> de local a aldebaran
# comprobar status de lxc local disabled: systemctl  status lxc*
# version : 0.3
# licencia: GPLv3

sudo systemctl stop lxc*
sudo /sbin/route add  -net 10.0.3.0 netmask 255.255.255.0  gw 192.168.1.140
