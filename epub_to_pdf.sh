#!/bin/bash
# version: 0.2
# by Claude&F5x
# rationale:
# license: GPL

read -p "Enter EPUB file path: " epub_file
read -p "Enter EPUB file name: " epub_name

pdf_file="${epub_file}/${epub_name}.epub"

ebook-convert "$epub_file/$epub_name.epub" "path/to/your/book.pdf"
