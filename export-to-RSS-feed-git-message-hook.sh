#!/bin/sh
#
# This script adds every pushed commit info to XML file
# Make sure to place it in your bare repo's /hooks dir and make it executable
#
# via https://gist.github.com/sergeylukin/2622308
#
# resulting onto feed.xml ? : 
# <?xml version="1.0" encoding="UTF-8"?>
# <feed xmlns="http://www.w3.org/2005/Atom" xmlns:thr="http://purl.org/syndication/thread/1.0" xml:lang="en">
# <title type="text">Git push feed</title>
# </feed>


git update-server-info

FILE=/path/to/feed.xml

# Get diapasone of pushed commits
while read oldrev newrev refname
do
  # Loop over pushed commits and concatenate XML data into $commits variable
  commits=$(git log $oldrev~..$newrev --pretty=format:'%h|%an|%s|%at' |
  while IFS='|' read hash author message timestamp
  do

    date=`date -d @$timestamp +%Y-%m-%dT%H:%M:%SZ`
    echo "\<entry\>\n\
      \<author\>\<name\>$author\<\/name\>\<\/author\>\n\
      \<title\>$message\<\/title\>\n\
      \<published\>$date\<\/published\>\n\
      \<summary type=\"html\"\>\n\
      \<![CDATA[\n\
      ]]\>\n\
      \<\/summary\>\n\
    \<\/entry\>"

  done)

  # Add $commits variable value after 3rd line in XML file
  sed -i "3 a $(echo $commits)" $FILE
done
