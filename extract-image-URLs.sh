#!/bin/bash
# source - https://stackoverflow.com/questions/71548851/how-to-extract-image-urls-using-bash
# context - sena55 - via USecue

lynx -dump -listonly -image_links -nonumbers "$1" |
grep -Ei '\.(jpg|png|gif)$' |
tr '\n' '\000' |
xargs -0 -- wget --no-verbose --

