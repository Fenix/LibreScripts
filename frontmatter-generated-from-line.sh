#!/bin/bash
# version: 0.2
# rationale:  script for frontmatter generation
# licencse : GPLv3 

#  given this text line command,
# ```
#     • Cloud Valley Hub, BIG SEE Best Commercial Building Award, 2022
# ```
# and given this template

# ```
# ---
# title: 
# year: 
# --- 

# Lorem Ipsum

# ```

# convert the text line  into a cloud-valley-hub.md file, like this

# ```
# ---
# title: Cloud Valley Hub
# year: 2022
# ---

# BIG SEE Best Commercial Building Award

# ```

# suggest a a bash script in order to accomplish it, following rules apply :

# 1.- input file with the referred text lines...  like "    • Cloud Valley Hub, BIG SEE Best Commercial Building Award, 2022" is in the follwing path : /home/fenix/precontent/awards.md 
# 2.- as said, each of the referred awards.md text line converts to the relevant new markdwon file to be place in this path : /home/fenix/precontent/awards/


# File paths
input_file="/home/fenix/precontent/awards.md"
output_folder="/home/fenix/precontent/awards"

# Create output folder if it doesn't exist
mkdir -p "$output_folder"

# Read input file line by line
while IFS= read -r line; do
  # Extract title, year, and award from the line
  title=$(echo "$line" | sed -E 's/• (.+),.*/\1/')
  year=$(echo "$line" | sed -E 's/.*([0-9]{4}).*/\1/')
  award=$(echo "$line" | sed -E 's/.*,(.*)/\1/')

  # Create the markdown file
  echo "---" > "$output_folder/$title.md"
  echo "title: $title" >> "$output_folder/$title.md"
  echo "year: $year" >> "$output_folder/$title.md"
  echo "---" >> "$output_folder/$title.md"
  echo "" >> "$output_folder/$title.md"
  echo "$award" >> "$output_folder/$title.md"
done < "$input_file"


# Make sure that the input file `awards.md` contains all the necessary lines before running the script.
#
# Once you've saved the revised script, make it executable using `chmod +x convert_awards.sh`. Then you can run the script by executing `./convert_awards.sh` in the terminal. It should read the input file, extract the relevant information, and generate separate markdown files for each award in the specified output folder.
