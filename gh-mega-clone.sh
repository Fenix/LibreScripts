#! /bin/bash
# version: 0.1
# rationale
# source: https://github.com/cli/cli/issues/2450
# download a maximum of 1000 @cferdinandi's repos and organize them under `./cferdinandi/`:
gh repo list cferdinandi --limit 1000 | while read -r repo _; do
  gh repo clone "$repo" "$repo"
done
