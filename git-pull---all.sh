#! /bin/sh

# git pull --all | sync todas las ramas desde el remoto a local
# a-la milkovsky https://stackoverflow.com/questions/4318161/can-git-pull-all-update-all-my-local-branches


for remote in `git branch -r`;
  do git branch --track $remote;
done

git fetch --all
git pull --all
