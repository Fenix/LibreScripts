# rationale:
#   combine Python's requests library with the html-to-markdown API to export website content into Markdown:
#   see https://html-to-markdown.com/api
# version : 0.1 UNTESTED
# license : WTFPL

import requests
from bs4 import BeautifulSoup

# Step 1: Web scrape the content of a webpage
def scrape_webpage(url):
    response = requests.get(url)
    if response.status_code == 200:
        return response.text
    else:
        raise Exception(f"Failed to retrieve the webpage. Status code: {response.status_code}")

# Step 2: Extract main content (optional step to clean the HTML using BeautifulSoup)
def extract_main_content(html):
    soup = BeautifulSoup(html, 'html.parser')
    # Extract specific tags or sections, e.g., main content
    main_content = soup.find('main') or soup.body
    return main_content.prettify() if main_content else html

# Step 3: Convert HTML to Markdown using html-to-markdown API
def html_to_markdown(html, api_key):
    api_url = "https://api.html-to-markdown.com/v1/convert"
    headers = {
        "X-API-Key": api_key,
        "Content-Type": "application/json",
    }
    payload = {"html": html}
    response = requests.post(api_url, headers=headers, json=payload)
    if response.status_code == 200:
        return response.json().get("markdown")
    else:
        raise Exception(f"API request failed. Status code: {response.status_code}, Message: {response.text}")

# Step 4: Save Markdown to a file
def save_markdown_to_file(markdown, file_path):
    with open(file_path, "w", encoding="utf-8") as file:
        file.write(markdown)

# Example Usage
if __name__ == "__main__":
    try:
        # Replace with your target webpage and API key
        target_url = "https://example.com"
        api_key = "YourApiKey"

        # Scrape and convert webpage content
        html_content = scrape_webpage(target_url)
        cleaned_html = extract_main_content(html_content)
        markdown_content = html_to_markdown(cleaned_html, api_key)

        # Save to a file
        save_markdown_to_file(markdown_content, "output.md")
        print("Content exported to Markdown successfully!")
    except Exception as e:
        print(f"An error occurred: {e}")
