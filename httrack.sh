#!/bin/bash
# version: 0.1
# author: fenix (at your service)
# rationale:
# web content scraping journey with HTTrack/ cURL / wget (at the end of the day ? ) :

# - capture, archive in markdown format (add a pandoc pipe httrack ).
# - Every output file has then name of the last part of the URL , e.g. : https://example.com/first-page -> turns to first-page.md 

#  Mimic / keep original website's relative link-structure, creating the relevant folders :

# e.g. https://example.com/section3/third-page -> turns to  ../section3/third-page.md  . Note the need of creating the relevant:
# |_ example.com
#    |_section1
#    |_section2
#    |_section3
#    |_ ...
# within the project folder : example.com

#
BASE_URL="https://example.org"
OUTPUT_DIR="example.com"

# Run HTTrack to download the website while keeping the original link structure
httrack "$BASE_URL" -O "$OUTPUT_DIR" --keep-links -W -s -n "*" -v

# Find all HTML files in the downloaded directory and convert them to Markdown
find "$OUTPUT_DIR" -name "*.html" | while read html_file; do
  # Extract the base name from the HTML file and create the Markdown filename
  markdown_file="${html_file%.html}.md"

  # Use pandoc to convert the HTML to Markdown
  pandoc "$html_file" -f html -t markdown -o "$markdown_file"

  # Optionally remove the original HTML file to save space
  rm "$html_file"
done

# Print completion message
echo "Website scraping and conversion complete. Files saved in $OUTPUT_DIR."
