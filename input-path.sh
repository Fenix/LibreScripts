#!/bin/bash
#
# rationale: Read a variable in bash with a default value
#  source: https://stackoverflow.com/questions/2642585/read-a-variable-in-bash-with-a-default-value


IN_PATH_DEFAULT="$HOME/Tmp/input.txt"
read -p "Please enter IN_PATH [$IN_PATH_DEFAULT]: " IN_PATH
IN_PATH="${IN_PATH:-$IN_PATH_DEFAULT}"

OUT_PATH_DEFAULT="$HOME/Tmp/output.txt"
read -p "Please enter OUT_PATH [$OUT_PATH_DEFAULT]: " OUT_PATH
OUT_PATH="${OUT_PATH:-$OUT_PATH_DEFAULT}"

echo "Input: $IN_PATH Output: $OUT_PATH"
