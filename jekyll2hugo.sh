#!/bin/bash
# rationale : import Jekyll -> Hugonizing -> HugoCMS @ Usecue
# license : GPLv3
# version 0.1 (alpha)


read -p "Enter the (JEKYLL) project/repo. folder name: " jekyll_source
read -p "Enter the (HUGO) project/repo. folder name : " hugo_dest

echo " We are gonna start migrating (JEKYLL) ... \n "
echo " ~/$jekyll_source \n"

echo "to (HUGO) 's repo at: \n"
echo "~/$hugo_dest \n"

# mkdir ~/$hugo_dest/content 
# mkdir ~/$hugo_dest/data
# mkdir ...


# via https://stackoverflow.com/questions/17674406/creating-a-full-directory-tree-at-once
 mkdir -p   mi-hugo/{static/{img,uploads},layouts/{_default,partials,shortcodes},data,content}



