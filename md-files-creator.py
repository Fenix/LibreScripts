#!/usr/bin/env python3
# version: 0.9
# license: GPLv3
#
# Rationale 
# Output :
# Created file: aamagir.md
# Created file: lrobinson.md
# Created file: adeekman.md
# Created file: mwestfa.md
# Created file: touaziz.md
# Created file: mbourik.md
# Created file: lolivier.md
# Created file: wkruijff.md
## 
# each file will have the correct front matter format, for example `wkruijff.md` will contain:

# --
# title: Wouter De Kruijff
# role: Manager Bedrijfsvoering
# image:
# ---



from pathlib import Path
from typing import Dict, Any
import sys
import logging

def setup_logging() -> None:
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(levelname)s - %(message)s'
    )

def validate_professional_data(data: Dict[str, Dict[str, str]]) -> bool:
    required_fields = {'first', 'last', 'role'}
    for username, info in data.items():
        if not all(field in info for field in required_fields):
            logging.error(f"Missing required fields for {username}")
            return False
        if not all(isinstance(v, str) for v in info.values()):
            logging.error(f"Invalid data types in {username}'s data")
            return False
    return True

def create_md_files(professionals: Dict[str, Dict[str, str]], output_dir: str = 'team') -> None:
    try:
        # Create output directory if it doesn't exist
        output_path = Path(output_dir)
        output_path.mkdir(exist_ok=True)
        
        if not validate_professional_data(professionals):
            raise ValueError("Invalid professional data structure")

        for username, info in professionals.items():
            file_path = output_path / f"{username}.md"
            try:
                with file_path.open('w', encoding='utf-8') as f:
                    f.write("---\n")
                    f.write(f"title: {info['first'].title()} {info['last'].title()}\n")
                    f.write(f"role: {info['role'].title()}\n")
                    f.write("image:\n")
                    f.write("---\n")
                logging.info(f"Created file: {file_path}")
            except IOError as e:
                logging.error(f"Failed to create {file_path}: {e}")
                continue
            
    except Exception as e:
        logging.error(f"An error occurred: {e}")
        sys.exit(1)

if __name__ == "__main__":
    professionals = {
        'aamagir': {
            'first': 'aisa',
            'last': 'amagir',
            'role': 'directeur-bestuurder',
        },
        'lrobinson': {
            'first': 'liesbeth',
            'last': 'robinson-hasewinkel',
            'role': 'programmacoördinator kennisdeling en toepassing',
        },
        'adeekman': {
            'first': 'amalia',
            'last': 'deekman',
            'role': 'programmacoördinator kennisontwikkeling',
        },
        'mwestfa': {
            'first': 'mireille',
            'last': 'westfa',
            'role': 'wijkwerkplaatscoördinator zuidoost',
        },
        'touaziz': {
            'first': 'tarik',
            'last': 'ouaziz',
            'role': 'wijkwerkplaatscoördinator nieuw-west',
        },
        'mbourik': {
            'first': 'mohamed',
            'last': 'bourik',
            'role': 'wijkwerkplaatscoördinator noord',
        },
        'lolivier': {
            'first': 'linda',
            'last': 'van der schatte olivier',
            'role': 'secretariaat en communicatie',
        },
        'wkruijff': {
            'first': 'wouter',
            'last': 'de kruijff',
            'role': 'manager bedrijfsvoering',
        }
    }
    
    setup_logging()
    create_md_files(professionals)
