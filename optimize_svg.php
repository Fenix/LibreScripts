<?php
foreach (glob("static/uploads/svgs/*.svg") as $filename) {
    $directory = substr($filename,0,-4);
    if (!file_exists($directory)) {
        mkdir($directory);
    }
    $filedata = file_get_contents($filename);
    $replacements = [];
    $filedata = preg_replace_callback('/<image(.*?)\/>/', function($match) use ($directory) {
        $width = 1200;
        if (preg_match('/width="([^"]+)"/', $match[0], $matches)) {
            //$width = round($matches[1]*2);
        }
        return preg_replace_callback('/data:image\/([a-z]+);base64,([^"]+)/',function($match) use ($directory, $width) {
            $data = base64_decode($match[2]);
            $im = imagecreatefromstring($data);
            $hash = md5($data);
            $ratio = $width / imagesx($im);
            $height = imagesy($im) * $ratio;
            $new_image = imagecreatetruecolor($width, $height);
            imagecopyresampled($new_image, $im, 0, 0, 0, 0, $width, $height, imagesx($im), imagesy($im));
            $filename = "$directory/$hash.jpg";
            imagejpeg($new_image,$filename,50);
            return preg_replace('/^static/','',$filename);
        }, $match[0]);
    },$filedata);
    file_put_contents($filename, $filedata);
}
