#!/bin/bash
# license: GPLv3
# version: 0.3

# Specify the folder path where docx files are located
folder="/home/fenix/precontent/MD-learning-WHAT-projects/"

# Loop through all docx files in the specified folder
for file in "$folder"/*.docx; do
    # Get the name of the file without extension
    filename=$(basename "$file" .docx)

    # Convert each docx file to markdown using pandoc
    pandoc -s "$file" -t markdown -o "$folder/$filename.md"
done
