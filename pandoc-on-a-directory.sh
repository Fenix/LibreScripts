#!/bin/bash

# Navigate to the specific folder containing markdown files
# cd /path/to/markdown/folder/
cd /tmp/stories


# Loop through each markdown file in the folder
for file in *.md; do
  # Generate the output filename by replacing the file extension with ".txt"
  output="${file%.md}.txt"

  # Perform the pandoc command on the current markdown file
  pandoc -f html -t markdown-native_divs-raw_html "$file" -o "$output"

  # Print a message to indicate the conversion is done for each file
  echo "Pandoc Converted $file to $output"
done
