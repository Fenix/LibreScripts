#! /usr/bin/bash
# invernar/reinicio/apagado i3wm 
# por jpa
r=$(echo -e "bloquear\ncerrar\ninvernar\nreiniciar\napagar\n" | dmenu -i -p ">>" -fn "magiquo-10" -nb "#000" -nf "#35556B")

case "$r" in
    "bloquear") slock;;
    "cerrar") /usr/bin/i3-msg exit;;
    "invernar") /usr/bin/systemctl suspend;;
    "reiniciar") /usr/bin/systemctl reboot;;
    "apagar") /usr/bin/systemctl shutdown;;
esac
