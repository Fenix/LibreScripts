#!/bin/bash
# rationale :  a script that given such 'senario79-ofztl3ro8d361b0cg6yv7stz79esjpgogd96scsjls.png' renames all  images in the folder to
#    a pattern like 'senario79.png' . Note that image file extensions can be jpg or png.
# v0.1 


# Iterate over jpg and png files
for file in *.jpg *.png; do
  # Continue only if file exists
  [ -e "$file" ] || continue

  # Extract the scenario number and file extension
  if [[ $file =~ (scenario[0-9]+).*\.(jpg|png) ]]; then
    scenario_number=${BASH_REMATCH[1]}
    extension=${BASH_REMATCH[2]}

    # Construct new filename
    new_filename="${scenario_number}.${extension}"

    # Rename the file
    mv -v "$file" "$new_filename"
  fi
done




