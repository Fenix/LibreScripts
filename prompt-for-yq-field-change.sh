#!/bin/bash
# name : "Prompt for Yq field(s?) to change"
# rationale : making our Yq commands even more flexible, by prompting the user for the fields to be changed
#  **   find -name  "*.md" -exec yq --front-matter="process" '.review="USECUED"' -i {} \;
#  should become something like
#  **   find -name  "*.md" -exec yq --front-matter="process" '.$FIELD="$VALUE"' -i {} \;
  
