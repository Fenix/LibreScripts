#!/bin/bash
#
# rationale: Read a variable in bash with a default value
#  source: https://stackoverflow.com/questions/2642585/read-a-variable-in-bash-with-a-default-value

# # fullname="USER INPUT"
# read -p "Enter fullname: " fullname
# # user="USER INPUT"
# read -p "Enter user: " user


# echo "you entered $user and $fullname"

# Now : Read a variable in bash with a default value


read -p "Enter your name [Richard]: " name
name=${name:-Richard}
echo $name
