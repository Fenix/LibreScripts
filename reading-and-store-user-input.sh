#!/bin/bash
# rationale: read and store user input
# source https://dev.to/zachgoll/introduction-to-bash-scripting-28no
#  https://stackoverflow.com/questions/18544359/how-do-i-read-user-input-into-a-variable-in-bash
 
# fullname="USER INPUT"
read -p "Enter fullname: " fullname
# user="USER INPUT"
read -p "Enter user: " user


echo "you entered $user and $fullname"
