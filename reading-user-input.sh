#!/bin/bash
# source https://dev.to/zachgoll/introduction-to-bash-scripting-28no

# Reading the user's input into the user_input variable
read user_input

echo "The user entered: $user_input"
