
#!/bin/bash
# You can use the following script in Bash to remove the line breaks

# Read the contents of file1 into a variable
text=$(<file1)

# Replace the line break with a space
text=${text//$'\n'/' '}

# Print the modified text
echo "$text"
