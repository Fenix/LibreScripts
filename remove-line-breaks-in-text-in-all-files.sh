#!/bin/bash
# # You can use the following script in Bash to remove the line breaks in all markdown files in a folder

# Set the folder path
folder="/tmp/stories2"

    # Loop through each text file
for file in "$folder"/*.txt; do
  # Read the contents of each file into a variable
  text=$(cat "$file")

  # Replace the line break with a space
  text=$(echo "$text" | tr '\n' ' ')

  # Overwrite the file with the modified text
  echo "$text" > "$file"
done


