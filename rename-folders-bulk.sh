#!/bin/bash
# rationale
# version: 0.1
# GPLv3
#    - The first sed expression: s/^[0-9]\. // removes a single digit followed by a dot and space from the beginning of the folder name if it exists. For example, "1. Folder" becomes "Folder".
#    - The second sed expression: s/[^A-Z]//g removes any characters that are not uppercase letters from the folder name. This ensures that only uppercase letters are preserved. For example, "Folder_2" becomes "Folder".

# That's it! The script renames folders based on certain patterns and then moves the remaining folders to another location.




destination="~/swp/precontent/_projects2"

for folder in */; do
    new_folder=$(echo "$folder" | sed 's/^[0-9]\. //; s/[^A-Z]//g')
    mv "$folder" "$new_folder"
done

mv [0-9]* "$destination"

