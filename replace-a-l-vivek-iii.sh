#!/bin/bash
# replacing -sculpitng markdown front-matter s
#   'uri: ' > 'permalink: /'
# 
# inspiration source :
# https://www.cyberciti.biz/faq/unix-linux-replace-string-words-in-many-files/
# 
# license: GPLv3
# script version 0.2

OLD="uri: "
NEW="permalink: \/"
DPATH="/tmp/smitwoning-products/*.md"
for f in $DPATH
do
  if [ -f $f -a -r $f ]; then
      sed -i "s/$OLD/$NEW/g" "$f"
      sed -i '2s/$/\//' "$f"
  else
   echo "Error: Cannot read $f"
  fi
done

