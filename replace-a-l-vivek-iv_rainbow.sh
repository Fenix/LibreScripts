#!/bin/bash
# replacing -sculpitng markdown front-matter s
#   'uri: ' > 'permalink: /'
# 
# inspiration source :
# https://www.cyberciti.biz/faq/unix-linux-replace-string-words-in-many-files/
# 
# license: GPLv3
# script version 0.4
OLD1=
NEW2=
OLD2="field_basic_page_subtitle: "
NEW2="image:"
DPATH="/home/jordi/Org/LATJ/infopages_dl-i.md/*.md"
for f in $DPATH
do
    if [ -f $f -a -r $f ]; then
      # DONE en v3 :	
      sed -i "s/$OLD/$NEW/g" "$f"
      # sed -i '2s/$/\//' "$f"
      # sed -i '3 a subtitle: with the Latinamericajourneys.guru ' "$f"
      # sed -i '4 a heading:' "$f"
      # sed -i '5 a layout: short-ipages' "$f"
      # sed -i "s/$OLD2/$NEW2/g" "$f"
      # sed -i '6 a description: >-' "$f"
      # sed -i '7 a icons: true' "$f"
  else
   echo "Error: Cannot read $f"
  fi
done

