#!/bin/bash
# replace-expression-in-all-files-in-dir-and-subdirs-recursively script - from Unix Grimmoire website + GPTcoop
# version: 0.1 
# Rationale :
#  prompt the user of the script, before running about 'foo' and bar', storing them in shell variables in the new script
# Prompt the user for the strings 'foo' and 'bar'
read -p "Enter the string to replace (foo): " replace_string
read -p "Enter the replacement string (bar): " replacement_string

# Run 'find' command and execute 'sed' with user-defined variables
find . -type f -exec sed -i "s/$replace_string/$replacement_string/g" {} +



# - The two `read` commands prompt the user for input, asking for the string to replace (defaulting to "foo") and the replacement string (defaulting to "bar").
# - The user's input is stored in the `replace_string` and `replacement_string` variables.
# - The `find` command remains the same, but within `sed`, we use double-quotes `"` instead of single-quotes `'` to allow variable interpolation.
# - The user-defined variables `$replace_string` and `$replacement_string` are used within the `sed` expression to perform the substitution based on the user's input.
