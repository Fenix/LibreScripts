#!/bin/bash
# will iterate through all Text files in the given folder and replace the line '## stitle' with
# '## Mijn verhaal'

# folder="/path/to/folder"
folder="/tmp/stories2"

# Loop through all markdown files in the folder
for file in "$folder"/*.txt; do
    # Substitute '## stitle' with '## Mijn verhaal'
    sed -i 's/## stitle/## Mijn verhaal/' "$file"
    
    # and remove all asterisks : '*'
    
    sed -i 's/*/ /g' "$file"

done
