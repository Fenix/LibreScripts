#!/bin/bash
# inspiration source :
# https://www.cyberciti.biz/faq/unix-linux-replace-string-words-in-many-files/
# 
# script version 0.3
# rationale : delete lines 4 to 6 and then line 9 and  line 10 from each file
# .. specified by `$f`

## refactor /en/posts/df
DPATH="/home/fenix/delferro_hugo/content/en/posts/df/*.md"
for f in $DPATH

do
  if [ -f $f -a -r $f ]; then
#       sed -i '4,6d' "$f"
      sed -i '6,7d' "$f"

  else
   echo "Error: Cannot read $f"
  fi
done

## refactor /en/posts/ba 
DPATH="/home/fenix/delferro_hugo/content/en/posts/ba/*.md"
for f in $DPATH

do
  if [ -f $f -a -r $f ]; then
#       sed -i '4,6d' "$f"
      sed -i '6,7d' "$f"

  else
   echo "Error: Cannot read $f"
  fi
done
