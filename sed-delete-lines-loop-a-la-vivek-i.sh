#!/bin/bash
# replacing -sculpitng markdown front-matter s
# borrar líneas 4 a 5
# 
# inspiration source :
# https://www.cyberciti.biz/faq/unix-linux-replace-string-words-in-many-files/
# 
# license: GPLv3
# script version 0.2

DPATH="/tmp/pages/*.md"
for f in $DPATH
do
  if [ -f $f -a -r $f ]; then
      sed -i '4,9d;2d' "$f"
  else
   echo "Error: Cannot read $f"
  fi
done

