#!/bin/bash
# massage markdown front-matter 
# delete lines 4 and 5
# 
# inspiration source :
# https://www.cyberciti.biz/faq/unix-linux-replace-string-words-in-many-files/
# 
# license: GPLv3
# script version 0.2

DPATH="/tmp/smitwoning-products/*.md"
for f in $DPATH
do
  if [ -f $f -a -r $f ]; then
      sed -i '4,5d' "$f"
  else
   echo "Error: Cannot read $f"
  fi
done

