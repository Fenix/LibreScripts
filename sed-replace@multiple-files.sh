#!/bin/bash
#
# change multiple files
#
# The following command is correctly changing the contents of 2 files.
# $ sed -i 's/abc/xyz/g' xaa1 xab1 
# for ?
#
# https://stackoverflow.com/questions/10445934/change-multiple-files
#
# 
for i in *.md; do
    sed -i 's/asd/dfg/g' $i
done

