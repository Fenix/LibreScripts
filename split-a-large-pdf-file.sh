#!/bin/bash
# source : Claude
# version 0.1
# rationale: The script will create two new files with "_part1" and "_part2"
## suffixes in the same directory as your input PDF with help of pdftk.deb

# Check if pdftk is installed in Debian

# Prompt for PDF path
read -p "Enter the full path to your PDF file: " pdf_path

# Check if file exists
if [ ! -f "$pdf_path" ]; then
    echo "Error: File does not exist!"
    exit 1
fi

# Get the directory and filename
dir_path=$(dirname "$pdf_path")
filename=$(basename "$pdf_path")
filename_noext="${filename%.*}"

# Get total number of pages
total_pages=$(pdftk "$pdf_path" dump_data | grep NumberOfPages | awk '{print $2}')

echo "Total pages in PDF: $total_pages"

# Prompt for split point
read -p "Enter the page number where you want to split (1-$total_pages): " split_point

# Validate split point - using test command instead of [[
if ! [ "$split_point" -eq "$split_point" ] 2>/dev/null || [ "$split_point" -lt 1 ] || [ "$split_point" -gt "$total_pages" ]; then
    echo "Error: Invalid page number!"
    exit 1
fi

# Create output filenames
output1="${dir_path}/${filename_noext}_part1.pdf"
output2="${dir_path}/${filename_noext}_part2.pdf"

# Split the PDF
echo "Splitting PDF..."
pdftk "$pdf_path" cat 1-"$split_point" output "$output1"
pdftk "$pdf_path" cat $((split_point + 1))-end output "$output2"

echo "Split complete!"
echo "First part: $output1 (pages 1-$split_point)"
echo "Second part: $output2 (pages $((split_point + 1))-$total_pages)"
