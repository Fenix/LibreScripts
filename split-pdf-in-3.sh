#!/bin/bash
# source : Claude
# version 0.2 debugging
# rationale: The script will create 3 new files with, by splitting the original one 
## suffixes in the same directory as your input PDF with help of pdftk.deb

# Check if file command is available
if ! command -v file &> /dev/null; then
    echo "Error: 'file' command not found. Please install it."
    exit 1
fi

# Prompt for PDF path
while true; do
    read -p "Enter the full path to your PDF file: " pdf_path
    
    # Check if file exists
    if [ ! -f "$pdf_path" ]; then
        echo "Error: File does not exist! Please try again."
        continue
    fi
    
    # Check if it's a valid PDF
+   echo "Checking if $pdf_path is a valid PDF file..."
    if ! check_pdf "$pdf_path"; then
        echo "Error: Not a valid PDF file! Please try again."
        continue
    fi
    
    # Try to get page count
+   echo "Trying to get the number of pages in the PDF file..."
    if ! total_pages=$(pdftk "$pdf_path" dump_data 2>/dev/null | grep NumberOfPages | awk '{print $2}'); then
        echo "Error: Could not read PDF file! Please check if the file is corrupted."
        continue
    fi
    
    # If we got here, we have a valid PDF
    break
done

# Get the directory and filename
dir_path=$(dirname "$pdf_path")
filename=$(basename "$pdf_path")
filename_noext="${filename%.*}"

echo "Total pages in PDF: $total_pages"

# Calculate split points
pages_per_part=$((total_pages / 3))
remainder=$((total_pages % 3))

# Calculate split points with remainder distribution
split1=$pages_per_part
split2=$((pages_per_part * 2))

# Adjust splits if there's a remainder
if [ $remainder -eq 1 ]; then
    split2=$((split2 + 1))
elif [ $remainder -eq 2 ]; then
    split1=$((split1 + 1))
    split2=$((split2 + 2))
fi

# Create output filenames
output1="${dir_path}/${filename_noext}_part1.pdf"
output2="${dir_path}/${filename_noext}_part2.pdf"
output3="${dir_path}/${filename_noext}_part3.pdf"

# Split the PDF with error checking
echo "Splitting PDF into three parts..."

if ! pdftk "$pdf_path" cat 1-"$split1" output "$output1" 2>/dev/null; then
    echo "Error: Failed to create part 1"
    exit 1
fi

if ! pdftk "$pdf_path" cat $((split1 + 1))-"$split2" output "$output2" 2>/dev/null; then
    echo "Error: Failed to create part 2"
    exit 1
fi

if ! pdftk "$pdf_path" cat $((split2 + 1))-end output "$output3" 2>/dev/null; then
    echo "Error: Failed to create part 3"
    exit 1
fi

echo -e "\nSplit complete!"
echo "First part: $output1 (pages 1-$split1)"
echo "Second part: $output2 (pages $((split1 + 1))-$split2)"
echo "Third part: $output3 (pages $((split2 + 1))-$total_pages)"

# Print page distribution
echo -e "\nPage distribution:"
echo "Part 1: $split1 pages"
echo "Part 2: $((split2 - split1)) pages"
echo "Part
