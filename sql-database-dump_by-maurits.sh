#!/bin/bash
# rationale: dumps and syncs a mysql database from the production server 
mkdir -p data
#rsync -avr vupress@ams01.usecue.nl:app/data/ data/
#rsync -avr vupress@ams01.usecue.nl:app/web/p/ web/p/
sudo echo Syncing DB... && ssh ams01.usecue.nl "sudo mysqldump --single-transaction vupress | gzip -f -c" | zcat | sudo mysql vupress
