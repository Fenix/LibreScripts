#!/bin/bash
# replacing -sculpitng markdown front-matter s
#   'image:' > 'image: ' \!/
# 
# inspiration source :
# https://www.cyberciti.biz/faq/unix-linux-replace-string-words-in-many-files/
# 
# license: GPLv3
# script version 0.2
##
# OLD=""
# NEW=""
DPATH="/home/jordi/Org/LATJ/infopages_dl-i.md/*.md"
for f in $DPATH
do
  if [ -f $f -a -r $f ]; then
#     sed -i "s/$OLD/$NEW/g" "$f"
      sed -i '5d' "$f"
  else
   echo "Error: Cannot read $f"
  fi
done

