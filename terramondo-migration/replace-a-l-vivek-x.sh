#!/bin/bash
# replacing -sculpitng markdown front-matter s
#   '# image:'NULL' ' > 'image: /uploads/images/'
# 
# inspiration source :
# https://www.cyberciti.biz/faq/unix-linux-replace-string-words-in-many-files/
# 
# license: GPLv3
# script version 0.2

DPATH="/home/jordi/Org/LATJ/infopages_dl-i.md/*.md"
for f in $DPATH
do
  if [ -f $f -a -r $f ]; then
      #      sed -i '12i image: /uploads/images/' "$f"
      sed  -i '10d' "$f"
      sed -i '10i image: /uploads/images' "$f"
  else
   echo "Error: Cannot read $f"
  fi
done

