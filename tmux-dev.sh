#!usr/bin/env bash
# author : Alexis Reigel
# source :  https://dev.to/koffeinfrei/easy-tmux-session-setup-2m75

# Create tmux session
tmux new-session -d

# window 0 rename and split horizontally
tmux rename-window MUSIC
tmux split-window -v
tmux send-keys 'mocp ~/Music/Waves/' C-m

##
## Start Emacs in the 2nd pane of window 0
##  tmux send-keys 'emacs' C-m

# Create a (2nd) window 1
tmux new-window
tmux rename-window HUGOCODEX
tmux send-keys 'cd ~/hugocodex/' C-m
tmux split-window -v
tmux send-keys 'cd ~/hugocodex/ && hugo serve -p 1111 --disableFastRenderP' C-m



# Create a (3rd) window 2
tmux new-window
tmux rename-window SYW_HUGO
tmux send-keys 'cd ~/syw_hugo/' C-m
tmux split-window -v
tmux send-keys 'cd ~/syw_hugo/ && hugo serve -p 1212 --disableFastRender' C-m


# Create a (4th) window 3
tmux new-window
tmux rename-window LATJ_HUGO
tmux send-keys 'cd ~/latj_hugo' C-m
tmux split-window -v
tmux send-keys 'cd ~/latj_hugo/ && hugo serve --disableFastRender' C-m


# ..

# ..


# Attach the session. REady to start hacking
tmux -2 attach-session -d
