#!/bin/bash
# https://how-to.dev/how-to-create-tmux-session-with-a-script

session="ensoñar"

tmux new-session -d -s $session

#window=0
#tmux rename-window -t $session:$window 'git'
# tmux send-keys -t $session:$window 'git fetch --prune --all' 
# tmux rename-window -t $session:$window 'MUSIC' 
# tmux send-keys -t $session:$window 'cd ~/Music' C-m

window=1
tmux new-window -t $session:$window -n 'MUSIC'
# tmux send-keys -t $session:$window 'npm run serve'


window=2
tmux new-window -t $session:$window -n 'hugocodex'
tmux send-keys -t $session:$window 'cd ~/hugocodex' C-m

window=3
tmux new-window -t $session:$window -n 'run'


tmux attach-session -t $session

