#!/bin/bash

cp /tmp/1-*sh $HOME
# Script para descargar y configurar la aplicacion vmail

umask 002
git clone https://github.com/omgslinux/vmail.git

cd vmail
dbhost=localhost
dbname=vmail
dbuser=vmail
dbpass=vmail
dbport='~'
sp='     '
secret=$(date|md5sum|awk '{ print $1 }')
sed "s/^.*database_host:.*$/\tdatabase_host: $dbhost/" app/config/parameters.yml.dist | sed "s/^.*database_port:.*$/\tdatabase_port: $dbport/" - | sed "s/^.*database_user:.*$/\tdatabase_user: $dbuser/" - | sed "s/^.*database_name:.*$/\tdatabase_name: $dbname/" - |sed "s/^.*database_password:.*$/\tdatabase_password: $dbpass/" - | sed "s/^.*secret:.*$/\tsecret: $secret/" > app/config/parameters.yml

sed "s/^.*database_host:.*$/${sp}database_host: $dbhost/" app/config/parameters.yml.dist | sed "s/^.*database_port:.*$/${sp}database_port: $dbport/" - | sed "s/^.*database_user:.*$/${sp}database_user: $dbuser/" - | sed "s/^.*database_name:.*$/${sp}database_name: $dbname/" - |sed "s/^.*database_password:.*$/${sp}database_password: $dbpass/" - | sed "s/^.*secret:.*$/${sp}secret: $secret/" > app/config/parameters.yml

composer install

bin/console doctrine:schema:update --force
bin/console vmail:setup

