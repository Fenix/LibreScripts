#!/bin/bash

#Script específico para deconstruyendo

mkdir -p /etc/ssl/private/deconstruyendo
cd /etc/ssl/private/deconstruyendo

echo "-----BEGIN CERTIFICATE-----
MIIGdTCCBF2gAwIBAgIJAIv9caEm4Ee8MA0GCSqGSIb3DQEBCwUAMIHIMQswCQYD
VQQGEwJFUzEOMAwGA1UECAwFU3BhaW4xDzANBgNVBAcMBk1hZHJpZDEYMBYGA1UE
CgwPRGVjb25zdHJ1eWVDT09QMS4wLAYDVQQLDCVBcHJlbmRlciBhIGRlc2FwcmVu
ZGVyIHkgYSBkZXNwcmVuZGVyMR8wHQYDVQQDDBZkZWNvbnN0cnV5ZW5kb2Nvb3Au
bmV0MS0wKwYJKoZIhvcNAQkBFh5kZWNvbnN0cnV5ZW5kb0BvcGVubWFpbGJveC5v
cmcwHhcNMTUxMjE0MTEwNjA0WhcNMjUxMjExMTEwNjA0WjCByDELMAkGA1UEBhMC
RVMxDjAMBgNVBAgMBVNwYWluMQ8wDQYDVQQHDAZNYWRyaWQxGDAWBgNVBAoMD0Rl
Y29uc3RydXllQ09PUDEuMCwGA1UECwwlQXByZW5kZXIgYSBkZXNhcHJlbmRlciB5
IGEgZGVzcHJlbmRlcjEfMB0GA1UEAwwWZGVjb25zdHJ1eWVuZG9jb29wLm5ldDEt
MCsGCSqGSIb3DQEJARYeZGVjb25zdHJ1eWVuZG9Ab3Blbm1haWxib3gub3JnMIIC
IjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA2O1s7yeRK0COnjE8E5vLUHLR
8pb81ojDu341Xd7n1aT+591dNunK4f490wMAsuoA5DUTBYFcorDGi3agAJCrvc83
QJ7i4Xms7x5fBhRRDuggUCl2SCByekxpyBDtHkji4g5CZIObNQSf6ch5PqQ48qWh
cVoOn3La/ZE0E9NwrfENKzCV072aBnP/3oIU+xY7gxN6HHPDknh0mBxJDZrkzm7Q
J6sh2rt+a8IqeTpDOwiUlHsTb6SV+hn6ypSa32fNJ1/5DDfWhU5+NHe/GFPsqti1
iN8aQZcgla4Bs/kZTXSWhwwW5V8sUc1u0JVfCYSvJs2FGAhLV3vpQj2fOJlBIdRz
6hAWRxJlfim+/FcDrrkABb37KtHQeqm4s+eihqrj+1xnQTkmLTe1FIV7xCT4vwCZ
hLhS+2fT9TKZrBigD+w7LLrXIPC6TQrol96U9wsK4N7OubI4kRgq+b+v2MgwpG2u
jOhupxrsHaS71D88jbhKFOvqGAZ7wd6SGjb2BuTnYBcjOP11+oDAkcPbUvxCrmwo
09R2iVgsJuojBioTJEjdNndjsoTZekoG6cHjj6w7vJxtojLdW7ge1Q9LJ6/YKTEh
6nlvcP7FAgVlSZZL5ZpQC1DKLD13ROzuidKeeMqStq5HtyAfDH4dJcptkTPCInY2
qf/+TgX4Iauo8sivKa8CAwEAAaNgMF4wHQYDVR0OBBYEFN2CL5Ycc8jisIi/o647
r2YB5PssMB8GA1UdIwQYMBaAFN2CL5Ycc8jisIi/o647r2YB5PssMA8GA1UdEwEB
/wQFMAMBAf8wCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEBCwUAA4ICAQCJJZm3G6hz
dW5C4HxbuvhSqubYLGtXThRjsxCT+regIYv4Jl0wVg/NGzb0E3DeT7UP2zycCfbc
nSkLfD+EM3NjiVLf3QJLrurSSl1yw71031Gd79vOqxymZ8HjEUpIgvOP0GvjxQ6J
sKAiRl5GWvLp03Ncph+h3U7jRkMuFXbUAC4fcjo3TI48+CiDk7GeDI+Sy7lXiJx8
F/pmeeiQxLWHnKKQywn5bxfnmYIaZxcF32nQKSmR6jQcpp8UoTvEDXpGSoD1vklV
CfkgnDXMCQpPnD7YN84TsPvElMPq0XuODM3SCpWtQnP5hL9VfCyMCbxEWlkKexRV
sEpDXBcekYk4prcfQpeZiq5hJ+aFdsIWr9MR56vmiayZAh0sgBxcIH4DhVVr6ybM
Ycs6BbogTv6bWguDnovZvOux9uNdZd6n9k7yucbQZKOuQQFLUEyy2XhKRGY32/Vb
9IPxDp4Uc7XB/a2T1DHExkl1AAlIy1/EVjo/yoEfBUVvca/NkxkTl9pX5aslT3Ok
BNsbcYca4zhGTqGMg3PTtGdDC0MC67hJbabF+9MDHgcFgkyEA46G5LCKSZHD1Zvq
Th3GMVv7mgv8DJzUwlqMDK3bQ+ndpgV6Ak20rxpOjI2l4hAhiFB0/uWbRYW1vieU
IYZYhbUk3Qjw96bpU2fs+El82RlKFm4YfA==
-----END CERTIFICATE-----
" > deconstruyendocoop_CA_rsa_4096_sha256.pem

ln -s deconstruyendocoop_CA_rsa_4096_sha256.pem deconstruyendocoop_CA.pem

echo "
-----BEGIN RSA PRIVATE KEY-----
MIIJKQIBAAKCAgEA2O1s7yeRK0COnjE8E5vLUHLR8pb81ojDu341Xd7n1aT+591d
NunK4f490wMAsuoA5DUTBYFcorDGi3agAJCrvc83QJ7i4Xms7x5fBhRRDuggUCl2
SCByekxpyBDtHkji4g5CZIObNQSf6ch5PqQ48qWhcVoOn3La/ZE0E9NwrfENKzCV
072aBnP/3oIU+xY7gxN6HHPDknh0mBxJDZrkzm7QJ6sh2rt+a8IqeTpDOwiUlHsT
b6SV+hn6ypSa32fNJ1/5DDfWhU5+NHe/GFPsqti1iN8aQZcgla4Bs/kZTXSWhwwW
5V8sUc1u0JVfCYSvJs2FGAhLV3vpQj2fOJlBIdRz6hAWRxJlfim+/FcDrrkABb37
KtHQeqm4s+eihqrj+1xnQTkmLTe1FIV7xCT4vwCZhLhS+2fT9TKZrBigD+w7LLrX
IPC6TQrol96U9wsK4N7OubI4kRgq+b+v2MgwpG2ujOhupxrsHaS71D88jbhKFOvq
GAZ7wd6SGjb2BuTnYBcjOP11+oDAkcPbUvxCrmwo09R2iVgsJuojBioTJEjdNndj
soTZekoG6cHjj6w7vJxtojLdW7ge1Q9LJ6/YKTEh6nlvcP7FAgVlSZZL5ZpQC1DK
LD13ROzuidKeeMqStq5HtyAfDH4dJcptkTPCInY2qf/+TgX4Iauo8sivKa8CAwEA
AQKCAgAijm/0CRGPGlGd0aoIVaElx6S6SGMuqcrEa26E7aVw2z/gH9thjAzNw3qF
ysyps61ArVPCZMElCZQXU2ZZsLIrIn9iw+zQjlmGHgh1J3diMIg1POWix8h0wwd2
33tmf6Xk7PCWooBvCbWPwGfQGskXnlsBe58ot1s7WY46OkyQ6P14C91SRbnrnT79
ayt8dCY20zt4nNhi1N19/cOFb09Ts0NljlVuSr/w2pV5ilZ7aPqCTLjj6AwcM1dI
PjQNsrNzuk4qLBr4UXC5WvoOHhBthugkTaYZHMIMFhQfX2cwd4ojKUHceDTPMzKb
96+UDJb6T3MMSQX3gb2+16ItmfxN5DrkQZVeE4NLONfx6IiyXmfqaeMBxxa/eoJH
xZtlzdzSuAGcz9p7+Cji7+//dAkHKyJxK2UaJ1zVS49nf4RaPFU2EGsdgZdwFoct
gG5mM3B5HJhD+COsdvNrnjEPAHngUS+FZ21EeqVqv+UU+GtcqKGu61202UUJ+TRG
ZLVKDuwmgNjUOmxfPQiIY41lfuy16n5NC3Gu8vK+gpR5dGUFvlWEIojZraLy65NN
psv8ORcvt8yYae9jo3/KzDNnYn40vDxEUdDkJU6HoXhaSJdDKb/tZJT8UQrs7par
k4VK8g25iRHG+5zlWksbOJZSrisI+5h9wZDv/BH0mHLgFF/wwQKCAQEA753mgEHx
y8zLcW2RHmxE3iUu8OgD9nv2kaLYzcgoyx/nMqz8Q+aJvnHdfRcj31fxPBQxituU
SHWMHoeuLfKooylPFNMS5cdvmxQC4VqLwKlXLZSdyjq0CYfGzZClTb9FOKRHAP0u
I7G/RCDAqLk5HjJR7ZqtPXCdd0tCqlKfOz0s81PduhuO6OXFx85SPliM4JBXNEzP
yxvAsdeHmdX2wch1e2tSBP+aHux5BiuonUj057AFZkG2bO0fuBbF/ldsoolWDSL3
mRpJ9Pmw0LTUbeGRXkZ3Igvs4ZTgq18Xh9at47A2gz+DnyiljZyQdZgzwub+ZY9b
LERQpjFCFiA0ewKCAQEA58JimAdYZJOg9p8686wi3Scg6JoGojtxCO/wfHK0vvD8
s1TtoIVhfwdOv70c77HKuRqA8Bun8eHs5N/k1WWQEloxeHnOkJ1LV1PL7Qv178ic
ZnP3BD+pyvZPvjf8taoLkHVqas+JpGX7YM5ad1Bl9odkgzL3qNNZzep02jsS0IzW
bP9LGPdQfMn9UaJ1KPfwuAXDmIspcyDXeYu9HrHYgR23kSHcg9tubUFypLn8gzFW
iqei6qBpyKhunz4aJLaBc03TcK7N/W+NGmzgvhkI6N+vyHnggjrwoFmbkF9knJAz
Y7fecgNo6lJUmPpQeSshWHLykNppNqOEpx+Y+r97XQKCAQB8zfqb/T0K0J16c/4t
IWlxZ16evcoqnQCddHuGG2Yb/n72ZyFx4GcIWdhqah4WG4EbsMzpLVt7rjYQRM1V
Q1nQuhhlmVbzdcBMvsyNQhMeg1VT+V+CULcra5wBwABJkYFlcjNFcZmRn+gmfQS8
HNWCQ+VmY7UuFjT+SKXtfuFPApsiFiGb6PSzm9I7MtyleovN/4BcTlHA0bC5MLCB
UO+YmWePpCNxVQyaZMLYku7dFX3yGz2EuF9jNtLZmIsB4dlEg1Hdv8PE6+tvfIo6
+qTWorGLsBonEp0MFfY7bGkeKJQjzsll7TQKNyqbqtuXVHJJZvVEQrnTQbXcCjYI
vjd3AoIBAQCIVPjapbeBsrZFXXW4UdJQdewN6E1usbjUoRC+pFte1zRP6e2/N7ca
mpnggMa82cF/EfA3To2E5HoOSQa74XrW0Oe5Dy5MM7tWK50SXK7X1xLp0d3FSg3/
rtVJ+HVYCCe1Wty26E73vmYZlEnzVgAk/ZBK/Xhif7GcSAdlOMrgk3ivtZ3fmdxn
DAtQMljpOLEKD5n36iT/7JnUjjc6DzRk/640Wc72YeabrpxZ+m5oXmWiOurDSonv
1+T7F/UZE6MiyO1QpAPYyf4fRV0PdnqfQraNH+TJtxJFwXCZhQQSdWFFj6vzNb5J
2RSayOT94jlDZpr6SwEJezdVeGjtWCahAoIBAQCB5gSTOvQ5/C02YNGLG0QHv5/R
FpZE9DLweboGpk0Y04OQUtMi2ZxquXQxqXW4vbVzC01Wbp/6Nd/HE4js0DYEDrnW
Mnuyy9ELefsytISz81VG2hM/6HtrZxTjkcmeX4X51xrQQb5vcO7BBlFPP/kXejs4
gqLvIJVEPkPz5LGobW1JNvNfMExO5TpBaTUkiXdLINi6rFUDgEpZgVvKmkFOUKdc
nFjjDIChF8ShMoLhwRTvha0SGAmjB5CqnqI5Olgedq0+XwcrogNSJHYmkSpwQK8+
4np99kp5PqZnP+PgabjsrUwrCx/hhCkEKVHQco7hcj0VoOQpwd6Iv7rDnLgy
-----END RSA PRIVATE KEY-----
" > wildcard.deconstruyendo.net-key.pem

echo "
-----BEGIN CERTIFICATE-----
MIIGizCCBHOgAwIBAgIJAIv9caEm4EfGMA0GCSqGSIb3DQEBCwUAMIHIMQswCQYD
VQQGEwJFUzEOMAwGA1UECAwFU3BhaW4xDzANBgNVBAcMBk1hZHJpZDEYMBYGA1UE
CgwPRGVjb25zdHJ1eWVDT09QMS4wLAYDVQQLDCVBcHJlbmRlciBhIGRlc2FwcmVu
ZGVyIHkgYSBkZXNwcmVuZGVyMR8wHQYDVQQDDBZkZWNvbnN0cnV5ZW5kb2Nvb3Au
bmV0MS0wKwYJKoZIhvcNAQkBFh5kZWNvbnN0cnV5ZW5kb0BvcGVubWFpbGJveC5v
cmcwHhcNMTYwMjAxMTUzMDAwWhcNMTcwMTMxMTUzMDAwWjCBqDELMAkGA1UEBhMC
RVMxDzANBgNVBAgMBk1hZHJpZDEPMA0GA1UEBwwGTWFkcmlkMRcwFQYDVQQKDA5E
ZWNvbnN0cnV5ZW5kbzETMBEGA1UECwwKRWdyb3Vwd2FyZTEaMBgGA1UEAwwRdnBz
MTkyNTA1Lm92aC5uZXQxLTArBgkqhkiG9w0BCQEWHmRlY29uc3RydXllbmRvQG9w
ZW5tYWlsYm94Lm9yZzCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBANjt
bO8nkStAjp4xPBOby1By0fKW/NaIw7t+NV3e59Wk/ufdXTbpyuH+PdMDALLqAOQ1
EwWBXKKwxot2oACQq73PN0Ce4uF5rO8eXwYUUQ7oIFApdkggcnpMacgQ7R5I4uIO
QmSDmzUEn+nIeT6kOPKloXFaDp9y2v2RNBPTcK3xDSswldO9mgZz/96CFPsWO4MT
ehxzw5J4dJgcSQ2a5M5u0CerIdq7fmvCKnk6QzsIlJR7E2+klfoZ+sqUmt9nzSdf
+Qw31oVOfjR3vxhT7KrYtYjfGkGXIJWuAbP5GU10locMFuVfLFHNbtCVXwmErybN
hRgIS1d76UI9nziZQSHUc+oQFkcSZX4pvvxXA665AAW9+yrR0HqpuLPnooaq4/tc
Z0E5Ji03tRSFe8Qk+L8AmYS4Uvtn0/UymawYoA/sOyy61yDwuk0K6JfelPcLCuDe
zrmyOJEYKvm/r9jIMKRtrozobqca7B2ku9Q/PI24ShTr6hgGe8Hekho29gbk52AX
Izj9dfqAwJHD21L8Qq5sKNPUdolYLCbqIwYqEyRI3TZ3Y7KE2XpKBunB44+sO7yc
baIy3Vu4HtUPSyev2CkxIep5b3D+xQIFZUmWS+WaUAtQyiw9d0Ts7onSnnjKkrau
R7cgHwx+HSXKbZEzwiJ2Nqn//k4F+CGrqPLIrymvAgMBAAGjgZUwgZIwDAYDVR0T
AQH/BAIwADAdBgNVHQ4EFgQU3YIvlhxzyOKwiL+jrjuvZgHk+ywwCwYDVR0PBAQD
AgXgMCMGA1UdEQQcMBqGGCouZGVjb25zdHJ1eWVuZG9jb29wLm5ldDARBglghkgB
hvhCAQEEBAMCBkAwHgYJYIZIAYb4QgENBBEWD3hjYSBjZXJ0aWZpY2F0ZTANBgkq
hkiG9w0BAQsFAAOCAgEAjdlpjvhJ9Dt1TbL0QoDKa9EnjofebnWfun44hjr29gPJ
Pk5Ye9GbSGiBNS654yz6u0B+6/LSgBF3BnN7Pn1q617PeRdqL1oNNr+0xScg4rfn
VegCgn4WCAhI7fUvgB2A2cENyK9gVUYi9CJqHN0z0TX2Dy8sPP4C/LtmbQuIX/qK
0o5dE8WINs6QwPM7JS1NfRcttdbM8XmOo5QsyrK97Am3tQH0PojmEI4sO5MxIFEx
uPp6tjcnDP4v6chc93WVn6O5IV0lDO9xHolhdk3O0OLO1U9Cl1fNaFDVh690aRHS
GQWJPwe/QpXVY7VZTkQuI4MaSiWSCEPY/GKHyAVQn0SWp2IW34WXmeJe+S0A2iwf
fG9upHNjc0+ryGcYDBkSGq/raHecMn6T11DFykesKrGi15RvxLS99ejhUC5FvT7p
NNcUHiTfJROcKiDTqb4zEnDAANsREqpEzL7xXvPV8AwIn8CvZ24Ihju3aWlNAwwS
ghurqALJ4mwMcebFcUoW8EQ3XGPaEVb6ffj4BPShtMs+JKx02lhxPSl6XJaVEB3m
A3OOJSheEvsVIcbTwYGFr2pJLvDnoqnp0NNQOuQdiwPw9M2zkaf4Mq5J0QTVfV0Y
1YEOw7BGoxxsxVklkhbYb9Cr+9mgCD+/VfC2VjNpcVNZ7kuX+KrWum0VqF0HQX0=
-----END CERTIFICATE-----
" > wildcard.deconstruyendo.net-cert.pem

cat deconstruyendocoop_CA.pem wildcard.deconstruyendo.net-key.pem > Deconstruyendo_CA-chain.pem
cat wildcard.deconstruyendo.net-cert.pem wildcard.deconstruyendo.net-key.pem > wildcard.deconstruyendo.net-certkey.pem

echo "-----BEGIN DH PARAMETERS-----
MIGHAoGBANfpqgSvXif6mwNR7mrY7iddEWeUYG5+h1YbhxNL/3cSZIwov7tCauSM
o/Ll7ZUtLgMmuMSCG4TExU231j5Mh3eI776yVRMNZM3bmw1MZy2P3GkbYnoT1oWC
XZ1ISsywhYUZyjHoDXZWnOON4EDo92CwBHbioS+Av1zo40LcyFqTAgEC
-----END DH PARAMETERS-----
" > dh1024.pem


# Ponemos certificados a nginx
sed 's@/etc/ssl/certs/ssl-cert-snakeoil.pem@/etc/ssl/private/deconstruyendo/Deconstruyendo_CA-chain.pem@' /etc/nginx/sites-available/$NAME.conf |
   sed 's@/etc/ssl/private/ssl-cert-snakeoil.key@/etc/ssl/private/deconstruyendo/wildcard.deconstruyendo.net-key.pem@' > /tmp/$NAME.conf
mv /tmp/$NAME.conf /etc/nginx/sites-available/$NAME.conf

service nginx restart

# A postfix

POSTCONF="
smtpd_sasl_auth_enable = yes
smtpd_sasl_authenticated_header = yes
smtpd_tls_CAfile = /etc/ssl/private/deconstruyendo/deconstruyendocoop_CA.pem
smtpd_tls_cert_file = /etc/ssl/private/deconstruyendo/wildcard.deconstruyendo.net-certkey.pem
smtpd_tls_security_level = may
"

while read linea;do
if [ ! -z "$linea" ];then
   echo "Añadiendo $linea"
   postconf -e "$linea"
fi
done <<< $POSTCONF

service postfix reload

# Courier

sed 's@^TLS_DHPARAMS=.*$@TLS_DHPARAMS=/etc/ssl/private/deconstruyendo/dh1024.pem@' /etc/courier/imapd-ssl | sed 's@^TLS_TRUSTCERTS=.*$@TLS_TRUSTCERTS=/etc/ssl/private/deconstruyendo/deconstruyendocoop_CA.pem@' - | sed 's@^TLS_CERTFILE=.*$@TLS_CERTFILE=/etc/ssl/private/deconstruyendo/wildcard.deconstruyendo.net-certkey.pem@' > /tmp/imapd-ssl

if [[ $(grep "^DEFAULT_DOMAIN" /etc/courier/authmysqlrc ) ]];then
   sed 's/^DEFAULT_DOMAIN.*$/DEFAULT_DOMAIN deconstruyendo.net/' /etc/courier/authmysqlrc > /tmp/authmysqlrc
   mv /tmp/authmysqlrc /etc/courier
else
   echo "DEFAULT_DOMAIN deconstruyendo.net" >> /etc/courier/authmysqlrc
fi


